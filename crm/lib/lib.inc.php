<?php
/**
 * Created by IntelliJ IDEA.
 * User: Иван
 * Date: 28.10.2017
 * Time: 12:52
 */

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\PHPMailer;

require_once 'var.inc.php';
require_once 'err.inc.php';
require_once 'mail/Exception.php';
require_once 'mail/SMTP.php';
require_once 'mail/PHPMailer.php';

class Data
{
    public static $data = null;

    private $cities;
    private $smtp;

    public static function getInstance()
    {
        if (!static::$data)
            self::$data = new Data();
        return self::$data;
    }

    private function __construct()
    {
        $this->cities = parse_ini_file('prop/city.property', true);
        $this->smtp = parse_ini_file('prop/smtp.property', true);
        if (!($this->cities && $this->smtp)) {
            include 'err.inc.php';
            exit(0);
        }
    }

    /**
     * @return  {Array[String]} список электронных адресов для отправки список электронных адресов для отправки
     */
    public function getListEmail()
    {
        return $this->smtp['emails']['list'];
    }

    /**
     * Возвращает список городов для отправки
     *  для данного получателя
     * @param {String} $email email адрес получателя
     * @return  {String[]} Список городов Список городов
     */
    public function getCities($email)
    {
        return $this->smtp[$email]['cities'];
    }

    public function getAllCities()
    {
        return $this->cities;
    }

    public function getSubject()
    {
        return $this->smtp["subject"];
    }

    public function getServerLogin()
    {
        return $this->smtp["login"];
    }

    public function getServerPassword()
    {
        return $this->smtp["password"];
    }

    public function getFrom()
    {
        return $this->smtp["from"];
    }

    public function getServerHost()
    {
        return $this->smtp["host"];
    }

    public function getServerPort()
    {
        return $this->smtp["port"];
    }

    public function getHumanNameCity($key)
    {
        return $this->getAllCities()[$key];
    }

}


/**
 * Очищает строку с номером телефона оставляя
 * только числа и заменяя лидирующую восьмерку на семерку
 *
 * @param $phoneNumber Номер телефона
 * @return {String} Номер телефона
 */
function clearPhone($phoneNumber)
{
    return preg_replace(['/\D/', '/^8/'], ['', '7'], $phoneNumber);
}

function getBasePhone($phoneNumber)
{
    return preg_replace(['/\D/', '/^[78]/'], ['', ''], $phoneNumber);
}


/**
 * Устанавливает значение глобальной переменной $currentReceiver
 * в соответсвии с ключем "label_name" из данных конфигурационного файла ".mappropertys"
 *
 * @param {String} $receiver - ID получателя указанного в конфигурационном файле
 */
function setCurrentReceiver($receiver)
{
    global $allProperty;
    global $currentReceiver;

    $label = $allProperty[$receiver]['label_name'];
    if ($label) {
        $currentReceiver = $label;
    } else {
        $currentReceiver = "Не уазан";
    }
}


/**
 * @param $bank ID получателя из ".mappropertys"
 * @return {String} URL получателя
 */
function getUrl($bank)
{
    global $allProperty;
    $url = $allProperty[$bank]['URL'];
    if (!$url) {
        return null;
    }
    return $url;
}

/**
 * Фомирует строку из массива
 * @param array $data
 * @param string &$string реультирующая строка
 * @param string $parent_key ключь вложенного массива
 */
function genKeyValue($data = array(), & $string = '', $parent_key = '')
{
    if (empty($data) && !empty($parent_key))
        $string .= "{$parent_key}=#";

    foreach ($data as $key => $value) {
        if ($key === 'HASH') continue;

        if (is_array($value) || is_object($value)) {
            genKeyValue($value, $string, $key);
        } else {
            $string .= "{$key}=" . ($value) . "#";
        }
    }
}

/**
 * проверяем подпись
 *
 * @param array $params Массив параметров в запросе, ключ = значение
 */
function getSign($params)
{
    $str = '';
    genKeyValue($params, $str);

    $str .= 'NatCredit';

    return md5($str);
}


/**
 * Проверяет и очищает данные введенные пользователем
 * @param string строковое значение для проверки
 * @return string очищенное и проверенное значение
 */
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}


function replaceValue($allData, $bank)
{
    global $allProperty;
    $values = $allProperty[$bank]['map_val'];
    if (!$values) return $allData;
    foreach ($allData as $key => $value) {
        $mappedValues = $values[$value];
        if ($mappedValues) {
            $allData[$key] = $mappedValues;
        }
    }
    return $allData;
}


/**
 * @param array $data Данные нашей формы
 * @param string $bank идентификатор банка в post.property
 * @return array Массив данных с замененными ключами нашей формы
 * на ключи контрагента, и добавочными ключами указанными в post.property
 */
function convertFormData($data, $bank)
{
    global $allProperty;
    $allField = $allProperty[$bank];
    $mapData = [];
    $staticData = [];

    if (!$allField) return $data;


    $mapField = $allField['map_key'];
    $staticFields = $allField['static_key'];

    if (!$mapField && !$staticFields) return $data;

    if ($mapField) {
        $mapData = replaceKey($data, $mapField);
    } else
        $mapData = $data;

    if ($staticFields) {
        $staticData = $staticFields;
    }
    $allData = array_merge($staticData, $mapData);
    $allData = replaceValue($allData, $bank);

    return $allData;
}


/**
 * Заменяет ключи именнованного массива
 * @param $data исходный массив
 * @param $mapField массив с отражением исходных ключей на новые
 * @return array возвращает массив с изменнеными ключами
 */
function replaceKey($data, $mapField)
{
    $result = [];

    foreach ($mapField as $key => $value) {
        $result[$value] = $data[$key];
    }

    return $result;

}


function initProperties()
{
    return parse_ini_file(FILE_MAP, true);
}



function request($method = 'POST', $url = '', $fields = array(), $headers = array())//Метод коннекта и передачи данных curl
{
    global $currentReceiver;
    $response = null;
    $cafile = __DIR__ . '/cacert.pem';
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_CAFILE, $cafile);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    if($method == 'POST')
        curl_setopt($ch, CURLOPT_POST, 1);

    if(!empty($fields))
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

    $isSuccess = curl_exec($ch);
    if (!$isSuccess) {
        $errstr = 'Не удалось произвести отправку ' . $currentReceiver . "\n"
            . 'ошибка cURL: "' . curl_errno($ch) . '"' . "\n"
            . 'сообщение cURL: ' . curl_error($ch) . "\n";
    }else{
        $response = $isSuccess;
    }

    curl_close($ch);

    return $response;
}


/**
 * @param array $data массив для отправки
 * @param string URL принимающего сервера
 * @param string $method ='POST' - метод для отправки
 * @return bool|string
 */
function send($data, $url, $method = "POST")
{
    global $currentReceiver;
    global $isErrorReporting;
    if (!$url) {
        trigger_error("URL for $currentReceiver is missing", E_USER_WARNING);
        $isErrorReporting = true;
        return false;
    }
    $cafile = __DIR__ . '/cacert.pem';
    $opts = [
        'http' => [
            'method' => $method,
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'content' => http_build_query($data),
            'timeout' => 60
        ],
        'ssl' => [
            "verify_peer" => true,
            "verify_peer_name" => true,
            'cafile' => $cafile,
        ]
    ];
    $context = stream_context_create($opts);
    $response = file_get_contents($url, false, $context);
    if ($response === false) {
        trigger_error("Сообщение $currentReceiver доставлено не было - сервер не ответил!", E_USER_WARNING);
        $isErrorReporting = true;
        return false;
    } elseif (!$response) {
        trigger_error("Сообщение $currentReceiver доставлено не было -  ответ сервера: '{$response}'", E_USER_WARNING);
        $isErrorReporting = true;
        return false;
    }
    return $response;
}


function getAllowedCity($bank)
{
    global $allProperty;
    $cities = $allProperty[$bank]['cityAllow'];
    return $cities;
}


function getDenyCity($bank)
{
    global $allProperty;
    $cities = $allProperty[$bank]['cityDeny'];
    return $cities;
}


function testPhone($phone)
{
    $result = '';
    $result = clearPhone($phone);
    $length = strlen($result);
    if ($length !== 11 || $result[0] != 7) {
        return null;
    }
    $result = preg_replace('/(7)(\d{3})(\d{3})(\d{2})(\d{2})/', '$1 ($2) $3-$4-$5', $result);
    $result = '+' . $result;
    return $result;
}

/**
 * Проверяет Наличие допустимых городов для отправки
 *
 * @param {String} $city Наименование города
 * @param {Array} $citiesAllowed Список допустимых городов для отправки
 * @return bool Отправлять или нет.
 */
function IsValidCity($city, $nameReceiver)
{
    $citiesAllowed = getAllowedCity($nameReceiver);
    $citiesDeny = getDenyCity($nameReceiver);

    $result = (in_array("ALL", $citiesAllowed) || in_array($city, $citiesAllowed));
    $result = (! in_array('ALL', $citiesDeny) && ! in_array($city, $citiesDeny));

    return $result;
}


/**
 * @param $result
 * @return mixed
 */
function decodingToUTF($result)
{
    $result = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
        return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
    }, $result);
    return $result;
}


/**
 * Формирует массив и отравляет
 * Отправляет значения
 * @param $data - Данные собственной формы
 *
 * @return bool|null
 */
function sendNatCredit($data)
{
    global $currentReceiver;
    global $isErrorReporting;

    if (!isValidCity($data['city'], NATIONALCREDIT)) return null;
    setCurrentReceiver(NATIONALCREDIT);
    $url = getUrl(NATIONALCREDIT);
    $allData = convertFormData($data, NATIONALCREDIT);
    $allData['TITLE'] = $allData['NAME'];
    $allData['HASH'] = getSign($allData);
    $response = send($allData, $url, "POST");

    $response = decodingToUTF($response);
    $_SESSION[NATIONALCREDIT] = $response;

    if ($response) { // если запрос от сервера вернулся
        $responseDecode = json_decode($response); // от НацКредит приходит в формате JSON
        if (!$responseDecode->success) { // Если были ошибки в форме
            trigger_error("Сообщение $currentReceiver доставлено не было! - $response",
                E_USER_WARNING);
            $isErrorReporting = true;
            return false;
        } else {
            // Здесь обрабатываем успешный результат отправки формы
            $result = json_encode($responseDecode, JSON_UNESCAPED_UNICODE);
            $date = gmdate("Y.m.d-H:i:s", time() + (60*60*3));
            $client = json_encode($data, JSON_UNESCAPED_UNICODE);
            $f = fopen(NATIONALCREDIT . "_sended.txt", "a");
            fwrite($f,"[$date]===--->>>{$result}===--->>>$client\n");
            fclose($f);

        }
    }
    return true;
}



function sendSouthLoaning($data)
{
    global $currentReceiver;
    global $isErrorReporting;
    $data['phone'] = clearPhone($data['phone']);
    if (!IsValidCity($data['city'], SOUTH_LOANING)) return null;
    setCurrentReceiver(SOUTH_LOANING);
    $url = getUrl(SOUTH_LOANING);
    $allData = convertFormData($data, SOUTH_LOANING);
    $result = send($allData, $url, "POST");
    $result = str_replace("'",'"',$result);
    $resultDecode = json_decode($result);
//    preg_match('/.?error.?:.?201.?/', $result)
    if ($resultDecode->error != 201) {
        trigger_error("Сообщение $currentReceiver доставлено не было! - $result",
            E_USER_WARNING);
        $isErrorReporting = true;
        return false;
    }else{
        $date = gmdate("Y.m.d-H:i:s", time() + (60*60*3));
        $client = json_encode($data, JSON_UNESCAPED_UNICODE);
        $f = fopen(SOUTH_LOANING . "_sending.txt", "a");
        fwrite($f,"[$date]===--->>>{$result}===--->>>$client\n");
        fclose($f);
    }
    $_SESSION[SOUTH_LOANING] = $result;
    return true;
}


function sendCredeo($data)
{
    global $currentReceiver;
    global $isErrorReporting;
    $data['phone'] = clearPhone($data['phone']);
    if (!isValidCity($data['city'], CREDEO)) return null;
    setCurrentReceiver(CREDEO);
    $url = getUrl(CREDEO);
    $convertData = convertFormData($data, CREDEO);

    $allData['data'] = $convertData;
    $allData['action'] = 'add';
    $allData['token'] = 'awdawdawd';

    $result = send($allData, $url, "POST");

    $result = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
        return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
    }, $result);
//    $resutltDecoding = json_decode($result);

    if ($result) { // если запрос от сервера вернулся
        $resutltdecode = json_decode($result); // от НацКредит приходит в формате JSON
        if (!$resutltdecode->success) { // Если были ошибки в форме
            trigger_error("Сообщение $currentReceiver доставлено не было! - $result",
                E_USER_WARNING);
            $isErrorReporting = true;
            return false;
        } else {
            // Здесь обрабатываем успешный результат отправки формы
            $result = json_encode($resutltdecode, JSON_UNESCAPED_UNICODE);
            $date = gmdate("Y.m.d-H:i:s", time() + (60*60*3));
            $client = json_encode($data, JSON_UNESCAPED_UNICODE);
            $f = fopen(CREDEO . "_sended.txt", "a");
            fwrite($f,"[$date]===--->>>{$result}===--->>>$client\n");
            fclose($f);
        }
    }
    $_SESSION[CREDEO] = $result;

    return true;
}

function sendMails($dataForm)
{
    $data = Data::getInstance();
    $emails = $data->getListEmail();
    $citiesAll = $data->getAllCities();
    $prettyCity = $citiesAll[$dataForm["city"]];


    $msg = <<<MSG
Имя: {$dataForm["name"]}
Телефон: {$dataForm["phone"]}
Город: {$prettyCity}
Сумма: {$dataForm["amount"]}
Примечание: {$dataForm['comment']}
MSG;

    foreach ($emails as $recipient) {
        $cityRecipients = $data->getCities($recipient);
        $isSend = isValidCity($dataForm['city'], $cityRecipients);
        if ($isSend)
            sendMail($data, $recipient, $msg);
    }

}

/**
 * @param {Data} $data Объект Data содежащий данные для подключения к серверу
 * @param $recipient Получатель
 * @param $msg Сообщение
 */
function sendMail(Data $data, $recipient, $msg)
{
    global $isErrorReporting;
    $mail = new PHPMailer(true);

    try {
//        if(!$data->getFrom()) throw new Exception("Не удалось получить email адрес получателя");
//        $mail->SMTPDebug = 2;
        $mail->CharSet = "UTF-8";
        $mail->isSMTP();
        $mail->Host = $data->getServerHost();
        $mail->Port = $data->getServerPort();
        $mail->SMTPAuth = true;
        $mail->Username = $data->getServerLogin();
        $mail->Password = $data->getServerPassword();

        $mail->setFrom($data->getFrom());
        $mail->addAddress($recipient);

        $mail->isHTML(false);
        $mail->Subject = $data->getSubject();
        $mail->Body = $msg;
        $mail->send();
    } catch (Exception $e) {
        trigger_error(
            "Письмо на адрес  $recipient доставлено не было! - {$e->errorMessage()}",
            E_USER_WARNING);
        $isErrorReporting = true;

    }
}

function sendMkk($data)
{
    global $currentReceiver;
    global $isErrorReporting;
    $dataConfig = Data::getInstance();
    $data['phone'] = getBasePhone($data['phone']);
    $data['phone'] = '7' . $data['phone'];
    if (!isValidCity($data['city'], MKK)) return null;
    setCurrentReceiver(MKK);
    $url = getUrl(MKK);
    $humanCityName = $dataConfig->getHumanNameCity($data['city']);

    $allData = convertFormData($data, MKK);
    $allData['city'] = $humanCityName;
    $queryString = http_build_query($allData);
    $url = $url . '?' . $queryString;
    $result = file_get_contents($url);
    $_SESSION[MKK] = $result;
    if (strcmp(substr($result, 3), "sended") != 0) {
        trigger_error("Сообщение $currentReceiver доставлено не было! - $result",
            E_USER_WARNING);
        $isErrorReporting = true;
        return false;
    }else{
        $date = gmdate("Y.m.d-H:i:s", time() + (60*60*3));
        $client = json_encode($data, JSON_UNESCAPED_UNICODE);
        $f = fopen(MKK . "_sended.txt", "a");
        fwrite($f,"[$date]===--->>>{$result}===--->>>$client\n");
        fclose($f);
    }

    return true;
}

function sendCarmoney($data)
{
    global $currentReceiver;
    global $isErrorReporting;
    $dataConfig = Data::getInstance();
    $data['phone'] = getBasePhone($data['phone']);
    $data['phone'] = '7' . $data['phone'];
    $data['amount'] = preg_replace('/[^\d]+/u', "", $data['amount']);
    if (!isValidCity($data['city'], CARMONEY)) return null;
    setCurrentReceiver(CARMONEY);
    $url = getUrl(CARMONEY);
    $humanCityName = $dataConfig->getHumanNameCity($data['city']);

    $allData = convertFormData($data, CARMONEY);
    if($data['city'] != 'ALL'){
        $allData['name'] .= " из города $humanCityName";
    }
    $allData['name'] = preg_replace('/[^\w\s\d]+/u', "", $allData['name']);

    $response = send($allData, $url);
    $decode_result = json_decode($response, true);
    if (!isset($decode_result["id"])) {
        trigger_error("Сообщение $currentReceiver доставлено не было! - $response",
            E_USER_WARNING);
        $isErrorReporting = true;
        return false;
    }else{
        $date = gmdate("Y.m.d-H:i:s", time() + (60*60*3));
        $client = json_encode($allData, JSON_UNESCAPED_UNICODE);
        $f = fopen(CARMONEY . "_sended.txt", "a");
        $response = decodingToUTF($response);
        fwrite($f,"[$date]===--->>>{$response}===--->>>$client\n");
        fclose($f);
    }


    return true;
}