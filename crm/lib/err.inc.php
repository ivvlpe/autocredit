<?php
/**
 * Created by IntelliJ IDEA.
 * User: Иван
 * Date: 31.10.2017
 * Time: 14:28
 */
function err_handler($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // Этот код ошибки не включен в error_reporting,
        // так что пусть обрабатываются стандартным обработчиком ошибок PHP
        return false;
    }
    $time = date("d-m-Y H:i:s");
    $str = "[$time] - $errstr in $errfile:$errline\n";
    switch ($errno) {

        case E_USER_ERROR:
            /*echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
            echo "  Фатальная ошибка в строке $errline файла $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Завершение работы...<br />\n";
         */   error_log($str,3, 'error.log');
            //exit(1);
            break;

        case E_USER_WARNING:
//            echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
            error_log($str,3, 'error.log');
            break;

        case E_USER_NOTICE:
//            echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
            error_log($str,3, 'error.log');
            break;

        default:
//            echo "Неизвестная ошибка: [$errno] $errstr<br />\n";
            error_log($str,3, 'error.log');
            break;
    }

    /* Не запускаем внутренний обработчик ошибок PHP */
    return true;
}

set_error_handler('err_handler');