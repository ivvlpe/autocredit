<?php

define('END_POINT', 'https://carmoney.ru/inc/send-form.php');
define('PARTNER', 'mezentsevis');

class Lead
{
    /** @var string  */
    public $title = '';

    /** @var string  */
    public $phone = '';

    /** @var int  */
    public $loan_sum = 0;

    /** @var int  */
    public $loan_month_count = 0;

    /** @var bool  */
    public $with_manager = false;
}

function sendLead(Lead $lead)
{
    $postFields = array(

        'fg'                => 1,
        'key'               => 'api',
        'partner_name'      => PARTNER,

        'name'              => $lead->title,
        'phone'             => $lead->phone,
        'loan_sum'          => (int)$lead->loan_sum,
        'loan_month_count'  => (int)$lead->loan_month_count,
        'with_manager'      => (bool)$lead->with_manager,

    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, END_POINT);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_POST, 1);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);

    return json_decode(curl_exec($ch));
}

// -------------------------------

$lead = new Lead();
$lead->title = 'Олег из г. Санкт-Петербург';
$lead->phone = '8 111 - 111-00-01'; // в любом формате
$lead->loan_sum = 75000;
$lead->loan_month_count = 36;

$result = sendLead($lead);
print_r($result);