<?php
/**
 * Created by IntelliJ IDEA.
 * User: Иван
 * Date: 31.10.2017
 * Time: 12:53
 */
/**
 * ключ для Национального кредита используется
 * в качестве названия раздела в файле post.property
 * для замены значений формы
 */
session_start();
define('NATIONALCREDIT', 'natcredit');


/**
 * ключ для Кредо используется
 * в качестве названия раздела в файле post.property
 * для замены значений формы
 */

define('CREDEO', 'credeo');

/**
 * ключ для МКК Содействие используется
 * в качестве названия раздела в файле post.property
 * для замены значений формы
 */
define('MKK', 'mkk_assistance');

/**
 * ключ для Юг-Автозайм используется
 * в качестве названия раздела в файле post.property
 * для замены значений формы
 */
define('SOUTH_LOANING', 'southLoaning');


/**
 * ключ для carmoney.ru используется
 * в качестве названия раздела в файле post.property
 * для замены значений формы
 */
define('CARMONEY', 'carmoney');

/**
 * Путь к файлу, с ключами и значениями
 * для из замены.
 */
define('FILE_MAP', 'prop/post.property');

/**
 * Путь к файлу со списком городов
 */
define('CITY_LIST', 'prop/city.property');


$allProperty = initProperties();

// Содержит Наименование получателя которому на
// на текущий момент происходит отправка
$currentReceiver = "";

// Если кому либо из получателей небыло доставлено
// сообщение устанавливается в true
$isErrorReporting = false;

$rootPath = $_SERVER['DOCUMENT_ROOT'];
