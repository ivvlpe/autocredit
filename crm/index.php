<?php
require_once 'lib/lib.inc.php';
$result = '';
$phone = '+7 ';
$name = $amount = $city = $comment = '';
$nameErr = $phoneErr = $amountErr = $city = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $name = test_input($_POST['name']);
    if (!$name) {
        $nameErr = "Имя не должно быть пустым";
    }

    $phone = test_input($_POST['phone']);
    $phone = testPhone($phone);
    if (!$phone) {
        $phoneErr = "Телефон должен содержать 11 цифр и начинаться либо с +7 или 8";
        $phone = "+7 ";
    }

    $amount = test_input($_POST['amount']);
    $city = test_input($_POST['city']);
    $comment = test_input($_POST['comment']);
    $data = ['name' => $name, 'phone' => $phone, 'amount' => $amount, 'city' => $city, 'comment' => $comment];

    if (!$nameErr && !$phoneErr) {
        try {
            sendNatCredit($data);
            sendSouthLoaning($data);
            sendCredeo($data);
            sendMails($data);
            sendMkk($data);
            sendCarmoney($data);
        } catch (Exception $e) {
            include "error.php";
            exit(0);
        }
        if ($isErrorReporting) {
            $_SESSION['sccClassResult'] = "w3-panel w3-red w3-card-4";
            $_SESSION['resultMessage'] = "Не все сообщения доставлены!";
        } else {
            $_SESSION['sccClassResult'] = "w3-panel w3-yellow w3-card-4";
            $_SESSION['resultMessage'] = "Все отправлено, спасибо!";
        }


        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        header("Location: $actual_link");
        exit(0);


    }

}


$data = Data::getInstance();
$cities = $data->getAllCities();


?>
    <!doctype html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Форма</title>
        <link rel="stylesheet" type="text/css" href="css/w3.css">
        <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-green.css">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="js/jquery.maskedinput.min.js"></script>


    </head>
    <body>
    <div class="w3-container w3-padding-large">
        <div class="w3-third w3-container w3-teme"></div>

        <div class="w3-container w3-third w3-center w3-round w3-card-4">

            <div class="w3-theme w3-card w3-round-large">
                <h2>Заявка</h2>

            </div>
            <form action="" method="post">
                <div>
                    <label>Город</label>
                    <div>
                        <select name="city" id="city" class="w3-select w3-round">
                            <?php
                            foreach ($cities as $key => $value) {
                                $sel = ($key === $city) ? 'selected' : '';
                                echo "<option value=\"{$key}\" {$sel} >{$value} </option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div>
                    <label>Имя</label>
                    <div>
                        <input class="w3-input w3-round" type="text" name="name" value="<? echo $name ?>">
                        <span class="w3-red"><?php echo $nameErr ?></span>
                    </div>
                </div>
                <div>
                    <label>Телефон</label>
                    <div>
                        <input class="w3-input w3-round phone" type="tel" name="phone" value="<? echo $phone ?>">
                        <span class="w3-red phone"><?php echo $phoneErr ?></span>
                    </div>
                </div>
                <div>
                    <label>Сумма</label>
                    <div><input class="w3-input w3-round amount" type="number" name="amount" value="<? echo $amount ?>">
                    </div>
                </div>
                <div>
                    <label>Комментарий</label>
                    <div>
                        <textarea class="w3-input w3-round amount" name="comment"><? echo $comment ?></textarea>
                    </div>
                </div>

                <div>
                    <label></label>
                    <div><input class="w3-round w3-btn w3-theme w3-margin-top" type="submit" value="Отправить заявку">
                    </div>
                </div>
            </form>
            <div class="<?php echo $_SESSION['sccClassResult'] ?>">
                <p><?php echo $_SESSION['resultMessage'] ?></p>
                <!--<p><?php /*echo $_SESSION[NATIONALCREDIT] */?></p>
                <p><?php /*echo $_SESSION[SOUTH_LOANING] */?></p>
                <p><?php /*echo $_SESSION[CREDEO] */?></p>
                <p><?php /*echo $_SESSION[MKK] */?></p>-->
            </div>
        </div>

        <div class="w3-container w3-third"></div>

    </div>
    <script>
        $('.phone').mask("+7 (999) 999-99-99");
    </script>
    </body>
    </html>
<?php

$_SESSION['sccClassResult'] = "";
$_SESSION['resultMessage'] = "";
$_SESSION[NATIONALCREDIT] = '';
$_SESSION[SOUTH_LOANING] = '';
$_SESSION[CREDEO] = '';
