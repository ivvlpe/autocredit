<?php

function sortSitysOnValues($fileName, $fileNameResult)
{
    $sytys = parse_ini_file($fileName);
    asort($sytys);
    $f = fopen($fileNameResult, "w");
    foreach ($sytys as $key => $value) {
        fwrite($f, "$key=$value\n");
    }
    fclose($f);
}

sortSitysOnValues('prop/city.property.bak', 'result.txt');

